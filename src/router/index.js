import Vue from 'vue';
import VueRouter from 'vue-router';

import auth from '../services/auth.js';

// import component yang dipakai sebagai router-view
import Login from '../components/Login'
import Admin from '../components/Admin'
import Home from '../components/Home';
import Users from '../components/Users';
import Jobs from '../components/Jobs';

// install vue-router
Vue.use(VueRouter);

// array config router
const routes = [
  {
    path: '/', // url, dalam kasus ini 'root'
    component: Login // component yang dipakai
  },
  {
    path: '/a',
    component: Admin,
    children: [
      {
        name: 'admin',
        path: '',
        component: Home
      },
      {
        name: 'users',
        path: 'users',
        component: Users
      },
      {
        name: 'jobs',
        path: 'jobs',
        component: Jobs
      }
    ]
  }
];

const router = new VueRouter({
  routes
})

export default router;






// const privateRoutes = [
//   '/a',
//   '/a/users',
//   '/a/jobs'
// ];

// const checkIsRoutePrivate = (route) => {
//   return privateRoutes.indexOf(route) > -1;
// }

// router.beforeEach((to, from, next) => {
//   if (!auth.getAuthState() && checkIsRoutePrivate(to.path)) {
//     next('/');
//   }
//   next();
// })